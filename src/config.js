const sparqlEndpoint = {
  // uri: 'http://localhost:8080/eal'
  uri: 'https://quit.easlit.erzw.uni-leipzig.de/eal',
  knowledgeMaps: 'https://quit.easlit.erzw.uni-leipzig.de/knowledgeMaps'
}

export { sparqlEndpoint }
