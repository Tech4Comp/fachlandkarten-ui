import Papa from 'papaparse'

function isCsvFile (file) {
  return file.name.endsWith('.csv') && (file.type === 'text/csv' || file.type === '')
}

async function isCsvValid (csvFile) {
  try {
    let data = await new Promise((resolve, reject) => {
      Papa.parse(csvFile, {
        complete: (results) => {
          if (results.errors.length !== 0)
            reject(results.errors)
          else
            resolve(results.data)
        }
      })
    })
    if (data[data.length - 1].length === 1) // remove empty last line
      data = data.slice(0, data.length - 1)
    const threeColumns = data.map((el) => el.length).find(el => el !== 3) === undefined
    const headerSpelling = data[0][0] === 'C1' && data[0][1] === 'C2' && data[0][2] === 'weight'
    return threeColumns && headerSpelling
  } catch (e) {
    return false
  }
}

export { isCsvFile, isCsvValid }
