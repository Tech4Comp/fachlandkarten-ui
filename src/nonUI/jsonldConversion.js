import isEmpty from 'lodash/isEmpty'
import isString from 'lodash/isString'
import isArray from 'lodash/isArray'
import get from 'lodash/get'
import last from 'lodash/last'
import { Set } from 'immutable' // List, Set, Record
// import chroma from 'chroma-js'
import jsonld from 'jsonld'

const context = {
  '@language': 'de',
  label: 'http://www.w3.org/2000/01/rdf-schema#label',
  relates: { '@id': 'http://halle/ontology/relates', '@type': '@id' },
  Term: 'http://halle/ontology/Term',
  Text: 'http://uni-leipzig.de/tech4comp/ontology/Text',
  contains: {
    '@id': 'http://uni-leipzig.de/tech4comp/ontology/contains',
    '@type': '@id'
  },
  centralTerm: {
    '@id': 'http://uni-leipzig.de/tech4comp/ontology/centralTerm',
    '@type': '@id'
  },
  Material: 'http://uni-leipzig.de/tech4comp/ontology/Material',
  continuativeMaterial: {
    '@id': 'http://uni-leipzig.de/tech4comp/ontology/continuativeMaterial',
    '@type': '@id'
  },
  title: 'http://uni-leipzig.de/tech4comp/ontology/title',
  format: 'http://uni-leipzig.de/tech4comp/ontology/format',
  link: {
    '@id': 'http://uni-leipzig.de/tech4comp/ontology/link',
    '@type': '@id'
  },
  Person: 'http://xmlns.com/foaf/0.1/Person',
  Definition: 'http://uni-leipzig.de/tech4comp/ontology/Definition',
  // defined: 'http://uni-leipzig.de/tech4comp/ontology/defined',
  defined: {
    '@id': 'http://uni-leipzig.de/tech4comp/ontology/defined',
    '@type': '@id'
  },
  developed: {
    '@id': 'http://uni-leipzig.de/tech4comp/ontology/developed',
    '@type': '@id'
  },
  about: {
    '@id': 'http://uni-leipzig.de/tech4comp/ontology/about',
    '@type': '@id'
  },
  LearningOutcome: 'http://tech4comp/eal/LearningOutcome',
  isAbout: {
    '@id': 'http://uni-leipzig.de/tech4comp/ontology/isAbout',
    '@type': '@id'
  },
  created: {
    '@id': 'http://uni-leipzig.de/tech4comp/ontology/created',
    '@type': '@id'
  },
  said: {
    '@id': 'http://uni-leipzig.de/tech4comp/ontology/said'
  },
  belongsTo: {
    '@id': 'http://uni-leipzig.de/tech4comp/ontology/belongsTo',
    '@type': '@id'
  },
  Critique: 'http://uni-leipzig.de/tech4comp/ontology/Critique',
  criticised: {
    '@id': 'http://uni-leipzig.de/tech4comp/ontology/criticised',
    '@type': '@id'
  },
  Statement: 'http://uni-leipzig.de/tech4comp/ontology/Statement',
  sameAs: {
    '@id': 'http://www.w3.org/2002/07/owl#sameAs',
    '@type': '@id'
  }
}
const defaultColor = '#808080'

// data = XML as String
async function convertFile (data) {
  data = await jsonld.compact(data, context)
  // console.log(data)
  let nodes = Set().asMutable()
  let links = Set().asMutable()

  data['@graph'].forEach(node => {
    if (!isEmpty(node['@type'])) {
      if (isType(node['@type'], 'Term')) {
        if (isType(node['@type'], 'Person'))
          addNode(nodes, node, 'Person', 'Person:\n', 'label', '#ff0000')
        else
          addNode(nodes, node, 'Term', 'Term:\n', 'label', '#000000')
        addEdges(links, node, 'relates', '#000000')
        addEdges(links, node, 'continuativeMaterial', '#039BE5')
        addEdges(links, node, 'defined', '#F57C00')
        addEdges(links, node, 'developed', '#ff0000')
        addEdges(links, node, 'created', '#ff0000')
        addEdges(links, node, 'criticised', '#F57C00')
        addEdges(links, node, 'said', '#F57C00')
      } else if (isType(node['@type'], 'Person'))
        addNode(nodes, node, 'Person', 'Person:\n', 'label', '#ff0000')
      if (isType(node['@type'], 'Text')) {
        addNode(nodes, node, 'Text', 'Text:\n', 'label', '#388E3C')
        addEdges(links, node, 'centralTerm', '#7B1FA2')
      }
      addNode(nodes, node, 'Material', 'Material:\n', 'title["@value"]', '#039BE5')
      if (isType(node['@type'], 'Definition')) {
        addNode(nodes, node, 'Definition', 'Definition:\n', '', '#F57C00')
        addEdges(links, node, 'about', 'F57C00')
      }
      if (isType(node['@type'], 'LearningOutcome')) {
        addNode(nodes, node, 'LearningOutcome', 'Learning Outcome:\n', '[@id]', '#55cdb2')
        addEdges(links, node, 'isAbout', '#55cdb2')
      }
      if (isType(node['@type'], 'Critique')) {
        addNode(nodes, node, 'Critique', 'Critique:\n', '', '#F57C00')
        addEdges(links, node, 'about', '#F57C00')
      }
      if (isType(node['@type'], 'Statement')) {
        addNode(nodes, node, 'Statement', 'Statement:\n', '', '#F57C00')
        addEdges(links, node, 'about', '#F57C00')
      }
    }
  })
  nodes = nodes.toList()
  links = links.toList()
  return { nodes: nodes.toJS(), edges: links.toJS() }
}

function isType (type, toCmompare) {
  if (isString(type))
    return type === toCmompare
  else if (isArray(type))
    return type.includes(toCmompare)
  else
    return false
}

function addNode (nodes, node, type = '', heading = '', namePath = '', color = defaultColor) {
  if (isType(node['@type'], type)) {
    let name = get(node, namePath)
    if (namePath === '[@id]' || (name !== undefined && name.startsWith('http://')))
      name = last(name.split('/')).replace('_', ' ') // NOTE quickfix for presentation
    name = heading + ((name === undefined) ? '' : name)
    if (name.endsWith(':\n'))
      name = name.slice(0, name.length - 2)
    nodes.add({ id: decodeURI(node['@id']), name, color, group: type })
  }
}

function addEdges (links, node, key, color = defaultColor) {
  if (!isEmpty(node[key])) {
    if (isString(node[key]) && (node[key].startsWith('http://') || node[key].startsWith('_:')))
      node[key] = [node[key]]
    if (!isArray(node[key]))
      return
    node[key].forEach(el => {
      links.add({ source: decodeURI(node['@id']), target: decodeURI(el), text: key, color, group: key })
    })
  }
}

export { convertFile }
