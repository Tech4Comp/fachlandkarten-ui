import jsonld from 'jsonld'
import isEmpty from 'lodash/isEmpty'
import { Set, Map } from 'immutable'
import { parseTurtle, writeRDF } from '@/common'

const tmitocarGraphContext = {
  label: { '@id': 'http://www.w3.org/2000/01/rdf-schema#label', '@language': 'de' },
  term: 'http://halle/ontology/Term',
  relates: 'http://halle/ontology/relates'
}

const categoriesNew = Map([
  ['foaf', 'http://xmlns.com/foaf/0.1/'],
  ['Schema', 'https://schema.org/'],
  ['DBpedia', 'https://dbpedia.org/ontology/']
])

async function isJSON (file) {
  if (file.name.endsWith('.json') && (file.type === 'application/json' || file.type === '')) {
    try {
      JSON.parse(await file.text())
      return true
    } catch (e) {
      return false
    }
  } else
    return false
}

async function isJSONLD (file) {
  if (file.name.endsWith('.jsonld') && (file.type === 'application/ld+json' || file.type === '')) {
    try {
      const json = JSON.parse(await file.text())
      const ld = await jsonld.compact(json, {})
      if (!isEmpty(ld['@graph']))
        return true
      else
        return false
    } catch (e) {
      return false
    }
  } else
    return false
}

async function turtleToJSONLD (turtle, compact = true) {
  const quads = await parseTurtle(turtle)
  const nquads = await writeRDF(quads, 'application/n-quads')
  const expanded = await jsonld.fromRDF(nquads, { format: 'application/n-quads' })
  if (compact)
    return jsonld.compact(expanded, tmitocarGraphContext)
  else
    return expanded
}

async function turtleToNquads (turtle) {
  return writeRDF(await parseTurtle(turtle), 'application/n-quads')
}

async function jsonldToNQuads (jsonldData) {
  return jsonld.toRDF(jsonldData, { format: 'application/n-quads' })
}

async function annotateGraphWithSpotlight (jsonldDoc, spotlightData) {
  let graphWithSets = convertToSets(jsonldDoc['@graph'])

  graphWithSets = annotateGraph(graphWithSets, spotlightData)

  const jsondldGraph = resolveSets(graphWithSets)

  const internalContext = jsonldDoc['@context']
  internalContext.sameAs = 'http://www.w3.org/2002/07/owl#sameAs'

  return { '@context': internalContext, '@graph': jsondldGraph }
}

function convertToSets (jsonldGraph) {
  return jsonldGraph.map((term) => {
    if (!Set.isSet(term['@type'])) {
      const original = term['@type']
      if (Array.isArray(original))
        term['@type'] = Set.of(...original).asMutable()
      else
        term['@type'] = Set.of(original).asMutable()
    }
    if (!Set.isSet(term.sameAs)) {
      const original = term.sameAs
      if (!isEmpty(original) && !Array.isArray(original))
        term.sameAs = Set.of(original).asMutable()
      else if (!isEmpty(original) && Array.isArray(original))
        term.sameAs = Set.of(...original).asMutable()
      else
        term.sameAs = Set().asMutable()
    }
    return term
  })
}

function resolveSets (jsonldGraphWithSets) {
  return jsonldGraphWithSets.map((term) => {
    term['@type'] = term['@type'].toJS()
    term.sameAs = term.sameAs.toJS()
    if (isEmpty(term.sameAs))
      delete term.sameAs
    return term
  })
}

function annotateGraph (graphWithSets, spotlightData) {
  spotlightData.Resources.forEach((spottedWord) => {
    spottedWord['@types'] = replaceTypes(spottedWord['@types'].split(','))
    graphWithSets = graphWithSets.map((term) => {
      if (term.label.toLowerCase() === spottedWord['@surfaceForm'].toLowerCase()) { // NOTE expects same characters, ignores upper-/lowercase
        if (spottedWord['@similarityScore'] > 0.9) {
          if (!isEmpty(spottedWord['@types']))
            term['@type'].concat(spottedWord['@types'])
          if (!isEmpty(spottedWord['@URI']))
            term.sameAs.add(spottedWord['@URI'])
        }
      }
      return term
    })
  })
  return graphWithSets
}

function replaceTypes (typeArray) {
  return typeArray.map((type) => {
    if (type.toLowerCase().startsWith('http'))
      return type
    else {
      const [schema, instance] = type.split(':')
      const filtered = categoriesNew.filter((_, key) => key === schema) // lowecer/uppercase is important
      if (filtered.size === 0)
        return undefined
      else
        return filtered.valueSeq().first() + instance
    }
  }).filter(el => !isEmpty(el))
}

export { isJSON, isJSONLD, turtleToJSONLD, jsonldToNQuads, turtleToNquads, annotateGraphWithSpotlight }
