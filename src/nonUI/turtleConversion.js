import last from 'lodash/last' // flattenDeep
import { parseTurtle } from '@/common'
import { Set } from 'immutable' // List, Set, Record

// file = html input file result
function isTurtleFile (file) {
  if (file.name.endsWith('.ttl') && (file.type === 'text/turtle' || file.type === ''))
    return true
  else
    return false
}
// data = XML as String
async function convertFile (data) {
  data = await parseTurtle(data)
  const subjects = Set().asMutable()
  let nodes = Set().asMutable()
  let links = Set().asMutable()
  const defaultNodeColor = '#EF6C00'
  const defaultEdgeColor = '#0D47A140'

  data.forEach(node => {
    // console.log(node) // find all subjects
    if (node.predicate.value !== 'http://www.w3.org/1999/02/22-rdf-syntax-ns#type') {
      subjects.add(node.subject.value)
      if (node.object.termType !== 'Literal')
        subjects.add(node.object.value)
    }
  })
  data.forEach(node => { // add all literals
    if (node.object.termType === 'Literal') {
      subjects.delete(node.subject.value)
      nodes.add({ id: node.subject.value, name: node.object.value, color: defaultNodeColor, group: 1 })
    }
  })
  // add everything else
  subjects.forEach((node) => {
    const name = node.startsWith('http://') ? last(node.split('/')).replace(/_/g, ' ') : node
    nodes.add({ id: node, name, color: defaultNodeColor, group: 1 })
  })
  nodes = nodes.toList()

  data.forEach(node => {
    if (node.object.termType !== 'Literal' && node.predicate.value !== 'http://www.w3.org/1999/02/22-rdf-syntax-ns#type')
      links.add({ source: node.subject.value, target: node.object.value, color: defaultEdgeColor, group: 1 })
  })
  links = links.toList()
  // console.log(links.toJS())
  return { nodes: nodes.toJS(), edges: links.toJS() }
}

export { isTurtleFile, convertFile }
