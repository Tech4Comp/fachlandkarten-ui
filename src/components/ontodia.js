import React from 'react'
import { faGraduationCap, faUser, faCircle, faList, faAlignJustify, faLayerGroup, faLink, faLongArrowAltLeft } from '@fortawesome/free-solid-svg-icons'
import last from 'lodash/last'
const Ontodia = require('ontodia')
const template = require('./ontodiaCustomStandardTemplate')
const N3Parser = require('rdf-parser-n3')
const JsonLdParser = require('rdf-parser-jsonld')

export default class ontodia extends React.Component {
  constructor (args) {
    super(args)
    this.alreadyPrinted = false
    // NOTE implemented here to not execute conversion a dozen times (like in iconResolver)
    this.graduationCapIcon = this.getBase64ImageOfFaIcon(faGraduationCap)
    this.userIcon = this.getBase64ImageOfFaIcon(faUser)
    this.circleIcon = this.getBase64ImageOfFaIcon(faCircle)
    this.listIcon = this.getBase64ImageOfFaIcon(faList)
    this.alignJustifyIcon = this.getBase64ImageOfFaIcon(faAlignJustify)
    this.layerGroupIcon = this.getBase64ImageOfFaIcon(faLayerGroup)
    this.linkIcon = this.getBase64ImageOfFaIcon(faLink)
    this.longArrowAltLeftIcon = this.getBase64ImageOfFaIcon(faLongArrowAltLeft)
  }

  componentDidMount () { // NOTE fix bad ontodia logo link
    const ontodiaLink = document.getElementsByClassName('ontodia-paper')[0].nextSibling
    const ontodiaImage = ontodiaLink.firstChild
    ontodiaImage.style.cursor = 'auto'
    ontodiaImage.style['pointer-events'] = 'none'
    ontodiaLink.parentNode.replaceChild(ontodiaImage, ontodiaLink)
  }

  onWorkspaceMounted (workspace) {
    if (!workspace) { return }
    // NOTE this is a bit hacky and might cause unwanted side effects
    if (this.alreadyPrinted) { return }

    const dataProvider = new Ontodia.RDFDataProvider({
      data: [
        {
          content: this.props.dataToShow,
          type: ((typeof this.props.dataToShow) === 'object') ? 'application/ld+json' : 'text/turtle',
          fileName: 'Wissenslandkarte.ttl'
        }
      ],
      acceptBlankNodes: true,
      dataFetching: false,
      parsers: {
        'application/ld+json': new JsonLdParser(),
        'text/turtle': new N3Parser()
      }
    })

    const graphBuilder = new Ontodia.GraphBuilder(dataProvider)
    const loadingGraph = graphBuilder.getGraphFromTurtleGraph(this.props.dataToShow)

    workspace.showWaitIndicatorWhile(loadingGraph)

    loadingGraph.then(({ diagram, preloadedElements }) => {
      const model = workspace.getModel()
      return model.importLayout({
        diagram,
        preloadedElements,
        dataProvider
      })
    }).then(() => {
      workspace.forceLayout()
      workspace.zoomToFit()
      this.alreadyPrinted = true

      if (this.props.dynamicModal) {
        // NOTE this is a bit hacky and might not always work
        const elements = document.getElementsByClassName('ontodia-element-layer')
        Array.from(elements).forEach((el) => {
          el.addEventListener('dblclick', () => {
            const link = document.getElementsByClassName('ontodia-halo__folow')[0]
            el.dispatchEvent(new CustomEvent('showModal', { bubbles: true, detail: link.href }))
          })
        })
      }
    })
  }

  render () {
    return (<Ontodia.Workspace ref={ this.onWorkspaceMounted.bind(this) } languages={[{ code: 'de', label: 'German' }]} language='de' elementTemplateResolver={ _ => template.CustomStandardTemplate } typeStyleResolver={ types => this.iconResolver(types) } viewOptions={{ onIriClick: (e) => window.open(e.iri) }}></Ontodia.Workspace>)
  }

  iconResolver (types) {
    if (types.indexOf('http://tech4comp/eal/LearningOutcome') !== -1) {
      return { icon: this.graduationCapIcon }
    } else if (types.indexOf('http://xmlns.com/foaf/0.1/Person') !== -1) {
      return { icon: this.userIcon, color: 'red' }
    } else if (types.indexOf('http://tech4comp/eal/SingleChoiceItem') !== -1) {
      return { icon: this.circleIcon, color: '#4caf50' }
    } else if (types.indexOf('http://tech4comp/eal/MultipleChoiceItem') !== -1) {
      return { icon: this.listIcon, color: '#00bcd4' }
    } else if (types.indexOf('http://tech4comp/eal/FreeTextItem') !== -1) {
      return { icon: this.alignJustifyIcon, color: '#ff5722' }
    } else if (types.indexOf('http://tech4comp/eal/ArrangementItem') !== -1) {
      return { icon: this.layerGroupIcon, color: '#3f51b5' }
    } else if (types.indexOf('http://tech4comp/eal/AssignmentItem') !== -1) {
      return { icon: this.longArrowAltLeftIcon, color: '#000000' }
    } else if (types.indexOf('http://tech4comp/eal/RemoteItem') !== -1) {
      return { icon: this.linkIcon, color: '#795548' }
    } else if (types.indexOf('http://halle/ontology/Term') !== -1) {
      return { color: 'black' }
    } else if (types.indexOf('http://xmlns.com/foaf/0.1/Person') !== -1) {
      return { color: 'red' }
    } else {
      return { color: 'grey' }
    }
  }

  getBase64ImageOfFaIcon (vueFaIcon) {
    const svgPathData = last(vueFaIcon.icon)
    const svgElement = htmlToElem(`<svg aria-hidden="true" focusable="false" viewBox="0 0 630 512" width=90 height=90><path fill="currentColor" d="${svgPathData}"/></svg>`)
    const serializedSVG = new XMLSerializer().serializeToString(svgElement)
    return 'data:image/svg+xml;base64,' + window.btoa(serializedSVG)

    function htmlToElem (htmlString) {
      const tmp = document.createElement('template')
      tmp.innerHTML = htmlString.trim()
      return tmp.content.firstChild
    }
  }
}
