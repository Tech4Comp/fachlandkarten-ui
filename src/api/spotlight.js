import ky from 'ky'

export default {

  async getTextAnnotations (text, confidence, language) {
    try {
      const formData = new FormData()
      formData.append('text', text)
      formData.append('confidence', confidence)
      formData.append('language', language)
      const spotlightData = await ky.post('/annotateWithSpotlight', {
        body: formData,
        timeout: false
      }).json()
      return spotlightData
    } catch (e) {
      if (e.response && e.response.statusCode !== 404)
        console.log(e)
      throw e
    }
  }
}
