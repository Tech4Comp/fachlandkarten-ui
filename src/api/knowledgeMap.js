import ky from 'ky'
import isEmpty from 'lodash/isEmpty'
import { decodeUmlauts } from '@/common'
const { sparqlEndpoint } = require('../config')
const jsonld = require('jsonld')
const md5 = require('md5')

const materialContext = {
  title: 'http://uni-leipzig.de/tech4comp/ontology/title',
  format: { '@id': 'http://uni-leipzig.de/tech4comp/ontology/format', '@container': '@set' },
  textComplexity: 'http://uni-leipzig.de/tech4comp/ontology/textComplexity',
  kind: 'http://uni-leipzig.de/tech4comp/ontology/kind',
  link: {
    '@id': 'http://uni-leipzig.de/tech4comp/ontology/link',
    '@type': '@id'
  },
  amountOfWords: 'http://uni-leipzig.de/tech4comp/ontology/amountOfWords',
  watchTime: {
    '@id': 'http://uni-leipzig.de/tech4comp/ontology/watchTime',
    '@type': 'http://www.w3.org/2001/XMLSchema#duration'
  },
  processingTime: {
    '@id': 'http://uni-leipzig.de/tech4comp/ontology/processingTime',
    '@type': 'http://www.w3.org/2001/XMLSchema#duration'
  },
  time: {
    '@id': 'http://uni-leipzig.de/tech4comp/ontology/readTime',
    '@type': 'http://www.w3.org/2001/XMLSchema#duration'
  }
}

export default {

  async getAvailableMaps () {
    const query = `
    CONSTRUCT { ?s ?p ?o }
    WHERE {
      ?s ?p ?o
    }`

    try {
      const response = await ky.post(sparqlEndpoint.knowledgeMaps, {
        headers: { 'Content-Type': 'application/sparql-query', Accept: 'application/ld+json' },
        body: query
      }).json()
      if (response['@graph'])
        return response['@graph']
      else
        return [response]
    } catch (e) {
      if (e.response && e.response.statusCode !== 404)
        console.log(e)
      return []
    }
  },

  async getWholeMap (graph, format = 'text/turtle') {
    return this.getSpecializedMap(graph, undefined, format)
  },

  async getSpecializedMap (graph, type, format = 'text/turtle') {
    let query
    if (type === 'centralTerms')
      query = `PREFIX mluo: <http://halle/ontology/>
        PREFIX ulo: <http://uni-leipzig.de/tech4comp/ontology/>
        PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
        CONSTRUCT {
          ?s a mluo:Term ;
             rdfs:label ?label .
          ?s2 a ulo:Topic ;
              rdfs:label ?label2 ;
              ulo:centralTerm ?o .
          ?o a mluo:centralTerm .
        } WHERE {
          GRAPH <${graph}> {
            ?s a mluo:Term ;
               rdfs:label ?label .
            optional {
            ?s2 a ulo:Text ;
                rdfs:label ?label2 ;
                ulo:centralTerm ?o .
            }
          }
        }`
    else if (type === 'continuativeMaterial')
      query = `PREFIX mluo: <http://halle/ontology/>
        PREFIX ulo: <http://uni-leipzig.de/tech4comp/ontology/>
        PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
        CONSTRUCT {
          ?s a mluo:Term, ?v ;
             rdfs:label ?label ;
             ulo:continuativeMaterial ?uri .
          ?uri a ulo:continuativeMaterial ;
             rdfs:label ?title .
        } WHERE {
          GRAPH <${graph}> {
            ?s a mluo:Term ;
               rdfs:label ?label .
            optional {
              BIND (UUID() AS ?uri)
              ?s ulo:continuativeMaterial [
                 ulo:title ?title
              ] .
              BIND ( IF (BOUND (?s), mluo:hasMaterial, "" )  as ?v  ) .
            }
          }
        }`
    else
      query = `PREFIX mluo: <http://halle/ontology/>
        CONSTRUCT { ?s ?p ?o } WHERE {
          GRAPH <${graph}> {
            ?s ?p ?o .
          }
        }`

    try {
      const response = await ky.post(sparqlEndpoint.knowledgeMaps, {
        headers: { 'Content-Type': 'application/sparql-query', Accept: format },
        body: query
      })

      if (format === 'application/json' || format === 'application/ld+json')
        return response.json()
      else
        return response.text()
    } catch (e) {
      if (e.response && e.response.statusCode !== 404)
        console.log(e)
      return ''
    }
  },

  async getSubjects (graph) {
    const query = `PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
      CONSTRUCT { ?s a ?o . ?s rdfs:label ?label . } WHERE {
        GRAPH <${graph}> {
          ?s a ?o ;
             rdfs:label ?label .
        }
        FILTER (!isBlank(?s))
        FILTER (LANG(?label) = "de")
      }`

    try {
      const response = await ky.post(sparqlEndpoint.knowledgeMaps, {
        headers: { 'Content-Type': 'application/sparql-query', Accept: 'application/n-triples' },
        body: query
      }).text()

      return jsonld.compact(await jsonld.fromRDF(response), { '@language': 'de', label: 'http://www.w3.org/2000/01/rdf-schema#label' })
    } catch (e) {
      if (e.response && e.response.statusCode !== 404)
        console.log(e)
      return ''
    }
  },

  async getCentralTerms (graph) {
    const query = `PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
      SELECT DISTINCT ?term WHERE {
        GRAPH <${graph}> {
          ?s a <http://uni-leipzig.de/tech4comp/ontology/Text> ;
             <http://uni-leipzig.de/tech4comp/ontology/centralTerm> ?term .
        }
      }`

    try {
      const response = await ky.post(sparqlEndpoint.knowledgeMaps, {
        headers: { 'Content-Type': 'application/sparql-query', Accept: 'application/json' },
        body: query
      }).json()

      const tmp = response.results.bindings.map((el) => el.term.value)

      return tmp
    } catch (e) {
      if (e.response && e.response.statusCode !== 404)
        console.log(e)
      return ''
    }
  },
  // TODO decodeUmlauts is a workaround because created Fachlandkarten contain Umlauts
  async alterCentralTerm (graph, term, del = false) {
    const query = `PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
      ${(del) ? 'DELETE' : 'INSERT'} {
        GRAPH <${graph}> {
          ?s <http://uni-leipzig.de/tech4comp/ontology/centralTerm> <${decodeUmlauts(term)}> .
        }
      }
      WHERE {
        GRAPH <${graph}> {
          ?s a <http://uni-leipzig.de/tech4comp/ontology/Text> .
        }
      }`

    try {
      return await ky.post(sparqlEndpoint.knowledgeMaps, {
        headers: { 'Content-Type': 'application/sparql-update' },
        body: query
      }).text()
    } catch (e) {
      if (!isEmpty(e.response) && e.response.status !== 404)
        console.log(e)
      throw e
    }
  },

  async getcontinuativeMaterial (graph, term) {
    const query = `PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
      PREFIX ulo: <http://uni-leipzig.de/tech4comp/ontology/>
      CONSTRUCT { ?material ?p ?o } WHERE {
        GRAPH <${graph}> {
          <${term}> ulo:continuativeMaterial ?material .
          optional {
            ?material ?p ?o .
          }
        }
      }`

    try {
      let response = await ky.post(sparqlEndpoint.knowledgeMaps, {
        headers: { 'Content-Type': 'application/sparql-query', Accept: 'application/ld+json' },
        body: query
      }).json()

      response = await jsonld.compact(response, materialContext)

      if (!isEmpty(response['@graph']))
        return response
      else {
        const tmp = JSON.parse(JSON.stringify(response))
        delete tmp['@context']
        return { '@context': response['@context'], '@graph': [tmp] }
      }
    } catch (e) {
      if (e.response && e.response.statusCode !== 404)
        console.log(e)
      return {}
    }
  },

  async addMaterial (graph, term, material) {
    const query = `PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
      PREFIX ulo: <http://uni-leipzig.de/tech4comp/ontology/>
      PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
      INSERT DATA {
        GRAPH <${graph}> {
          <${term}> ulo:continuativeMaterial [
            a ulo:Material ;
            ulo:title "${material.title}" ;
            ulo:link <${material.link}> ;
            ulo:format ${material.format.map(el => '"' + el.toUpperCase() + '"').join(',')} ;
            ${(!isEmpty(material.time)) ? `ulo:processingTime "${material.time}"^^xsd:duration ;` : ''}
            ${(!isEmpty(material.textComplexity)) ? `ulo:textComplexity "${material.textComplexity}" ;` : ''}
            ${(typeof (material.amountOfWords) === 'number') ? `ulo:amountOfWords "${material.amountOfWords}"^^xsd:integer` : ''}
          ] .
        }
      }`

    try {
      return await ky.post(sparqlEndpoint.knowledgeMaps, {
        headers: { 'Content-Type': 'application/sparql-update' },
        body: query
      }).text()
    } catch (e) {
      if (!isEmpty(e.response) && e.response.status !== 404)
        console.log(e)
      throw e
    }
  },

  async alterMaterial (graph, term, material, original) {
    const query = `PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
      PREFIX ulo: <http://uni-leipzig.de/tech4comp/ontology/>
      DELETE {
        GRAPH <${graph}> {
          ?material ?p ?o .
        }
      } WHERE {
        GRAPH <${graph}> {
          <${term}> ulo:continuativeMaterial ?material .
          ?material ulo:title "${original.title}" ;
            ulo:link <${original.link}> ;
            ?p ?o .
        }
      }`

    try {
      await ky.post(sparqlEndpoint.knowledgeMaps, {
        headers: { 'Content-Type': 'application/sparql-update' },
        body: query
      }).text()
      return this.addMaterial(graph, term, material)
    } catch (e) {
      if (!isEmpty(e.response) && e.response.status !== 404)
        console.log(e)
      throw e
    }
  },

  async hasTopic (graph) {
    const query = `PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
      SELECT DISTINCT ?term WHERE {
        GRAPH <${graph}> {
          ?s a <http://uni-leipzig.de/tech4comp/ontology/Text> .
        }
      }`

    try {
      const response = await ky.post(sparqlEndpoint.knowledgeMaps, {
        headers: { 'Content-Type': 'application/sparql-query', Accept: 'application/json' },
        body: query
      }).json()

      return !isEmpty(response.results.bindings)
    } catch (e) {
      if (e.response && e.response.statusCode !== 404)
        console.log(e)
      return ''
    }
  },

  async getAllTerms (graph) {
    const query = `PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
      SELECT DISTINCT ?term WHERE {
        GRAPH <${graph}> {
          ?term ?p ?o .
          FILTER(!isBlank(?term))
        }
      }`

    try {
      const response = await ky.post(sparqlEndpoint.knowledgeMaps, {
        headers: { 'Content-Type': 'application/sparql-query', Accept: 'application/json' },
        body: query
      }).json()

      const tmp = response.results.bindings.map((el) => el.term.value)

      return tmp
    } catch (e) {
      if (e.response && e.response.statusCode !== 404)
        console.log(e)
      return []
    }
  },

  async createTopic (graph, name) {
    let terms = await this.getAllTerms(graph)
    terms = terms.map((term) => '<' + term + '>')
    const query = `PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
      INSERT DATA {
        GRAPH <${graph}> {
          <http://uni-leipzig.de/tech4comp/domainmodel/${encodeURIComponent(name)}> a <http://uni-leipzig.de/tech4comp/ontology/Text> ;
             rdfs:label "${name}"@de ;
             <http://uni-leipzig.de/tech4comp/ontology/contains> ${terms.join(', ')} .
        }
      }`
    try {
      return await ky.post(sparqlEndpoint.knowledgeMaps, {
        headers: { 'Content-Type': 'application/sparql-update' },
        body: query
      }).text()
    } catch (e) {
      if (!isEmpty(e.response) && e.response.status !== 404)
        console.log(e)
      throw e
    }
  },

  async createTerm (graph, name) {
    const query = `PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
      PREFIX mlum: <http://halle/domainmodel/>
      PREFIX mluo:  <http://halle/ontology/>
      INSERT DATA {
        GRAPH <${graph}> {
          mlum:${encodeURIComponent(name)} a mluo:Term ;
            rdfs:label "${name}"@de .
        }
      }`
    try {
      return await ky.post(sparqlEndpoint.knowledgeMaps, {
        headers: { 'Content-Type': 'application/sparql-update' },
        body: query
      }).text()
    } catch (e) {
      if (!isEmpty(e.response) && e.response.status !== 404)
        console.log(e)
      throw e
    }
  },

  async uploadNewKnowledgeMap (dataBlob, name) {
    const formData = new FormData()
    formData.append('file', dataBlob, 'data.nq')
    const graphName = 'http://tech4comp.de/knowledgeMap/' + md5(await dataBlob.text())

    try {
      await ky.post(sparqlEndpoint.knowledgeMaps + '/data?graph=' + graphName, {
        body: formData,
        timeout: false
      })
    } catch (e) {
      if (e.response && e.response.statusCode !== 404)
        console.log(e)
      return false
    }
    try {
      const query = `PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
      INSERT DATA { <${graphName}> rdfs:label '${name}' . }`

      await ky.post(sparqlEndpoint.knowledgeMaps, {
        headers: { 'Content-Type': 'application/sparql-update', Accept: 'text/plain' },
        body: query
      })
      return graphName
    } catch (e) {
      if (e.response && e.response.statusCode !== 404)
        console.log(e)
      return ''
    }
  },

  async deleteKnowledgeMap (graph) {
    try {
      const query = `DROP GRAPH <${graph}>`
      await ky.post(sparqlEndpoint.knowledgeMaps, {
        headers: { 'Content-Type': 'application/sparql-update' },
        body: query
      }).text()
    } catch (e) {
      if (!isEmpty(e.response) && e.response.status !== 404)
        console.log(e)
      throw e
    }
    try {
      const query = `DELETE {
        ?graph ?p ?o .
      } WHERE {
        ?graph ?p ?o.
        filter ( ?graph = <${graph}>)
      }`

      await ky.post(sparqlEndpoint.knowledgeMaps, {
        headers: { 'Content-Type': 'application/sparql-update', Accept: 'text/plain' },
        body: query
      })
    } catch (e) {
      if (e.response && e.response.statusCode !== 404)
        console.log(e)
      return ''
    }
  }
}
