import * as child from 'child_process'
import fileUpload from 'express-fileupload'
import bodyParser from 'body-parser'
import * as fs from 'fs'
import * as path from 'path'
import first from 'lodash/first'
import last from 'lodash/last'
import got from 'got'
import timeout from 'connect-timeout';

const querystring = require('querystring')

const projectRootPath = process.env.PWD

export default (app) => {
  app.use(timeout('10m'))
  app.use(fileUpload({ createParentPath: true }))
  app.use(bodyParser.json())
  app.use(bodyParser.urlencoded({ extended: true }))

  app.post('/postToTMitocar', async (req, res) => {
    const tmpPath = createTmpPath()

    const authfilePath = path.join(projectRootPath, '/auth.json')
    let textfilePath = ''
    if (req.files && req.files.textfile)
      textfilePath = path.join(tmpPath, (req.files.textfile.mimetype === 'text/plain') ? 'textfile.txt' : 'textfile.pdf')
    else
      textfilePath = path.join(tmpPath, 'textfile.txt')
    try {
      if (req.files && req.files.textfile)
        await req.files.textfile.mv(textfilePath)
      else
        fs.writeFileSync(textfilePath, req.body.text)
      const processOutput = child.execSync(`${projectRootPath}/tmitocar/tmitocar.sh -i ${textfilePath} -a ${authfilePath} -o ${req.body.format} -L ${req.body.language} -w ${req.body.wordspec} -s -r`)
      const resultfilePath = last(first(processOutput.toString().split('\n').filter((line) => line.startsWith('Saved received file as'))).split(' '))
      res.sendFile(resultfilePath, {}, (err) => {
        if (err) {
          try { res.status(500).send(err) } catch (error) {}
        } else
          fs.rmSync(tmpPath, { recursive: true })
      })
    } catch (err) {
      try { res.status(500).send(err) } catch (error) {}
    }
  })

  app.post('/getTextFromPDF', async (req, res) => {
    const tmpPath = createTmpPath()

    let textfilePath = ''
    if (!req.files || !req.files.textfile)
      res.send({ status: false, message: 'No file uploaded' })
    else {
      textfilePath = path.join(tmpPath, 'textfile.pdf')
      try {
        await req.files.textfile.mv(textfilePath)
        const processOutput = child.execSync(`${projectRootPath}/tmitocar/getTextFromPDF.sh -i ${textfilePath} -l ${req.body.language}`)
        const resultfilePath = last(first(processOutput.toString().split('\n').filter((line) => line.startsWith('Text successfully extracted and saved as'))).split(' '))
        res.sendFile(resultfilePath, {}, (err) => {
          if (err)
            try { res.status(500).send(err) } catch (error) {}
          else
            fs.rmSync(tmpPath, { recursive: true })
        })
      } catch (err) {
        try { res.status(500).send(err) } catch (error) {}
      }
    }
  })

  app.post('/convertModelToRDF', async (req, res) => {
    const tmpPath = createTmpPath()

    if (!req.files)
      res.send({ status: false, message: 'No file uploaded' })
    else {
      const modelfilePath = path.join(tmpPath, 'model.csv')
      try {
        await req.files.model.mv(modelfilePath)
        const processOutput = child.execSync(`cd ${projectRootPath}/rdf-transformation/ && ./transcode.sh -i ${modelfilePath} -m weights -c`)
        const resultfilePath = last(first(processOutput.toString().split('\n').filter((line) => line.startsWith('wrote results to'))).split(' '))
        res.sendFile(resultfilePath, (err) => {
          if (err) res.status(500).send(err) // ; else fs.rmSync(tmpPath, { recursive: true })
        })
      } catch (err) {
        res.status(500).send(err)
      }
    }
  })

  app.post('/annotateWithSpotlight', async (req, res) => {
    try {
      const queryString = querystring.encode({ text: req.body.text, confidence: req.body.confidence })
      const uri = `https://api.dbpedia-spotlight.org/${req.body.language}/annotate`
      const result = await got.post(uri, {
        headers: {
          accept: 'application/json',
          'content-type': 'application/x-www-form-urlencoded',
          'Content-Length': queryString.length
        },
        body: queryString
      })
      const parsed = JSON.parse(result.body)
      res.send(parsed)
    } catch (err) {
      console.log(err)
      res.status(500).send(err)
    }
  })
}

function createTmpPath () {
  const currentPath = path.join('/tmp', (new Date()).getTime().toString())
  fs.mkdirSync(currentPath)
  return currentPath
}
