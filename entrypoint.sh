#!/bin/sh

env | grep -i SERVICE_

cat /nodeApp/config.js.template | envsubst > ./src/config.js
npm run build
# TODO vue-cli-service is needed to execute project, move to dependies
#npm prune --production

exec "$@"
