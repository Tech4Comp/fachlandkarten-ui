const webpack = require('webpack')

module.exports = {
  pluginOptions: {
    express: {
      shouldServeApp: true,
      serverDir: './srv'
    }
  },
  devServer: {
    proxy: 'http://127.0.0.1:3000'
  },
  configureWebpack: {
    resolve: {
      fallback: {
        // fs: false,
        url: require.resolve('url/'),
        util: require.resolve('util/'),
        // assert: require.resolve('assert/'),
        http: require.resolve('stream-http'),
        // zlib: require.resolve('browserify-zlib'),
        // path: require.resolve('path-browserify'),
        https: require.resolve('https-browserify'),
        stream: require.resolve('stream-browserify'),
        crypto: require.resolve('crypto-browserify')
        // querystring: require.resolve('querystring-es3')
      }
    },
    module: {
      rules: [
        {
          test: /\.mjs$/,
          include: /node_modules/,
          type: 'javascript/auto'
        }
      ]
    },
    plugins: [
      new webpack.ProvidePlugin({
          Buffer: ['buffer', 'Buffer'],
      }),
      new webpack.ProvidePlugin({
          process: 'process/browser',
      }),
  ],
  }
}
