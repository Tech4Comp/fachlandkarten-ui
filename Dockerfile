# See https://github.com/rmeissn/tech4comp-dockerfiles for the base image description
FROM node:18
LABEL maintainer="Roy Meissner <meissner@informatik.uni-leipzig.de>"

ARG BUILD_ENV=LOCAL
ENV VIRTUAL_PORT=${VIRTUAL_PORT:-80}
ENV NODE_OPTIONS=--openssl-legacy-provider

RUN mkdir /nodeApp
WORKDIR /nodeApp

# ---------------- #
#   Installation   #
# ---------------- #

RUN apt update && apt install -y gettext jq tesseract-ocr tesseract-ocr-deu ruby openjdk-11-jre raptor2-utils bash file coreutils curl poppler-utils sed hunspell hunspell-en-gb hunspell-en-us hunspell-de-de catdoc locales
RUN gem install docsplit-ng
RUN sed -i 's/# en_GB.UTF-8/en_GB.UTF-8/g' /etc/locale.gen && sed -i 's/# de_DE.UTF-8/de_DE.UTF-8/g' /etc/locale.gen && locale-gen
ENV LANG en_GB.UTF-8
ENV LANGUAGE en_GB:en
ENV LC_ALL en_GB.UTF-8

RUN cd / && wget https://github.com/tarql/tarql/releases/download/v1.2/tarql-1.2.tar.gz && tar -xf tarql-1.2.tar.gz && rm tarql-1.2.tar.gz && ln -s /tarql-1.2/bin/tarql /bin/tarql

COPY ./ ./
RUN if [ "$BUILD_ENV" != "CI" ] ; then rm -R node_modules ; npm install --force ; fi
#RUN npm run build # moved to entrypoint script

# ----------------- #
#   Configuration   #
# ----------------- #

# ----------- #
#   Cleanup   #
# ----------- #

#RUN rm -R node_modules && npm cache clean --force

# -------- #
#   Run!   #
# -------- #

#TODO override config.js or read from env
ENTRYPOINT ["./entrypoint.sh"]
CMD npm run express:run
